## Umgebung (Environment)

Anstelle von Docker kann man gut mit `virtualenv` und `PyCharm` arbeiten.

### IDE
Falls `PyCharm` zu groß ist, könnt ihr auch andere IDEs wie `Visual Studio Code` verwenden. 
Als Studenten der RWTH Aachen können wir uns kostenlos bei JetBrains anmelden ([hier](https://www.jetbrains.com/de-de/community/education/#students)), um `PyCharm` herunterzuladen ([hier](https://www.jetbrains.com/pycharm/download/)).

### Python
#### Version
Wir verwenden Python Version 3.11. Falls es einen Grund gibt, eine andere Version zu nutzen, können wir sie austauschen. Um die Version zu überprüfen, führt folgendes aus:
```bash
which python3.11 && python3.11 --version
```
Es könnte sein, dass ihr es unter einem anderen Namen im PATH gespeichert habt oder dass es überhaupt nicht im PATH vorhanden ist. Wenn das der Fall ist, müsst ihr das beachten und in den Skripten immer `python3.11` durch den richtigen Namen ersetzen.

#### Virtualenv
Um `virtualenv` zu nutzen, müssen Sie es zunächst herunterladen:
```bash
python3.11 -m pip install virtualenv
```
Dann erstellen Sie den `virtualenv`-Ordner:
```bash
python3.11 -m virtualenv venv
```
Schließlich müssen Sie das `virtualenv` aktivieren, bevor Sie Module installieren oder ein Programm ausführen:
```bash
source venv/bin/activate
```

### Alles Zusammen
Wenn Sie `PyCharm` öffnen, können Sie mit einem Rechtsklick auf den Ordner `DBIS` oder wie auch immer er benannt wurde und dann auf `New > Jupyter Notebook` klicken. Bevor Sie etwas ausführen, sollten Sie unten links auf `Python ###` klicken und dann auf `Add New Interpreter > Add Local Interpreter` gehen, dann auf `Existing` klicken, und automatisch sollte der `venv`-Ordner gefunden werden. Nun sollte alles in Ordnung sein, und Sie können alle Übungen bearbeiten, wie Sie möchten.

## Git
Wir entwickeln hier keine komplexen Anwendungen, daher können wir direkt auf `main` pushen. Falls wir eine alternative Lösung haben als bereits von jemand anderem geschrieben, erstellen wir bitte einen Branch für diese Lösung und benennen ihn so, dass klar wird, worum es geht. Dann können wir, wenn wir diese Lösung diskutieren wollen, einen `diff` zwischen den Branches durchführen, um die Unterschiede zu sehen.